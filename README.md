## Technologies Used
.Net Core 2
RabbitMQ as an eventbus with RawRabbit libraries
MongoDB as a database for schemaless storage (fast prototyping)
Docker to run the solution
Dependency Injection Framework 

##  Structure

This is a dead simple that allows to create users and activities and authenticate users

### Actio.Common 
Jwt Authentication 
